import bpy

print("Renderer ---------------------------------")

group = bpy.data.objects["SR_Group"]
camera_index = 0
if group is not None:
    for child in group.children:
        if child.type == "CAMERA":
            scene = bpy.context.scene
            scene.camera = child
            scene.render.filepath = "//render/camera" + str(camera_index)
            bpy.ops.render.render(write_still=True)
            camera_index += 1
