import bpy

print("Camera switch ---------------------------------")

group = bpy.data.objects["SR_Group"]
camera_index = 0
target_camera_index = 0
selected_a_camera = False

if group is not None:
    for child in group.children:
        if child.type == "CAMERA":
            if child.select:
                target_camera_index = camera_index + 1
                print("Set camera index to : " + str(target_camera_index))
                child.select = False
            if camera_index == target_camera_index:
                if target_camera_index is not 0:
                    print("Setting scene camera to " + str(target_camera_index))
                    child.select = True
                    bpy.context.scene.camera = child
                    selected_a_camera = True
                    break
            camera_index += 1

if selected_a_camera is not True:
    print("Selecting any camera")
    for obj in group.children:
        if obj.type == "CAMERA":
            bpy.context.scene.camera = obj
            obj.select = True
            break

for area in bpy.context.screen.areas:
    if area.type == 'VIEW_3D':
        area.spaces[0].region_3d.view_perspective = 'CAMERA'
        break