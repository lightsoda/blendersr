# TODO
#
# 1) Selected object should be focus
#
# 2) Place cameras or locators around based on user input, number of steps/360
#
#    Could also be a single camera setup parented to a center locator that you
#    rotate based on current step
#
#    Offset start angle optio
#
# 3) Render views to single or multiple images
#
#    Have an option for offset, i.e. which view/step do we render first
#
#
#
#

bl_info = {
    "name": "360 sprite render",
    "category": "Object",
}

import bpy, math, mathutils

class BL360SpriteRender(bpy.types.Operator):
    """360 Sprite Render"""
    bl_idname = "object.bl360spriterender"
    bl_label = "360 Sprite Render"
    bl_options = {"REGISTER", "UNDO"}

    master_camera_name = "SR_MasterCamera"
    parent_empty_name = "SR_Group"
    target_empty_name = "SR_ConstraintTarget"
    child_camera_name = "SR_ChildCamera"

    distance = bpy.props.IntProperty(name="Distance", default=2, min=1, max=100)
    num_cameras = bpy.props.IntProperty(name="Cameras", default=8, min=1, max=100)
    camera_offset = bpy.props.FloatVectorProperty(name = "Camera offset", default=(0,0,0), min=-100, max=100, subtype="XYZ")
    group_rotation = bpy.props.FloatVectorProperty(name = "Group rotation", default=(0,0,0), min=-100, max=100, subtype="XYZ")
    
    def create_empty(self, location=(0.0,0.0,0.0), name="Empty"):
        new_empty = bpy.data.objects.new(name, None)
        bpy.context.scene.objects.link(new_empty)
        new_empty.location = location
        return new_empty

    def create_cameras(self):
        parent_empty = self.create_empty(name=self.parent_empty_name)
        target_empty = self.create_empty(name=self.target_empty_name)
        target_empty.parent = parent_empty

        # todo just assign vector?
        parent_empty.rotation_euler = mathutils.Euler((self.group_rotation.x, self.group_rotation.y, self.group_rotation.z), 'XYZ')

        ## Create master camera
        ## todo move this into initial position
        master_camera = bpy.data.cameras.new(self.master_camera_name)
        master_camera_object = bpy.data.objects.new(self.master_camera_name, master_camera)
        bpy.context.scene.objects.link(master_camera_object)

        for i in range(self.num_cameras):
            x = target_empty.location.x + self.camera_offset.x
            y = target_empty.location.y + self.camera_offset.y
            z = target_empty.location.z + self.camera_offset.z
            
            angle = math.radians(360 * (i / self.num_cameras))

            x += math.cos(angle) * self.distance
            y += math.sin(angle) * self.distance

            child_camera_object = master_camera_object.copy()
            child_camera_object.name = self.child_camera_name
            bpy.context.scene.objects.link(child_camera_object)
            child_camera_object.location = (x, y, z)
            child_camera_object.parent = parent_empty

            ## Create camera tracking constraint
            if target_empty is not None:
                camera_constraint = child_camera_object.constraints.new("TRACK_TO")
                camera_constraint.target = target_empty
                camera_constraint.track_axis = "TRACK_NEGATIVE_Z"
                camera_constraint.up_axis = "UP_Y"

    def execute(self, context):
        print("---------------------------------------")

        self.create_cameras()
        
        return {"FINISHED"}

def menu_func(self, context):
    self.layout.operator(BL360SpriteRender.lb_idname)

def register():
    bpy.utils.register_class(BL360SpriteRender)
    bpy.types.VIEW3D_MT_object.append(menu_func)

def unregister():
    bpy.utils.unregister_class(BL360SpriteRender)
    bpy.types.VIEW3D_MT_object.remove(menu_func)

if __name__ == "__main__":
    register()